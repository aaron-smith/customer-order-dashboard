-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.19 - MySQL Community Server - GPL
-- Server OS:                    Linux
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table dashboard.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_status_id` bigint unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customers_customer_status_id_foreign` (`customer_status_id`),
  CONSTRAINT `customers_customer_status_id_foreign` FOREIGN KEY (`customer_status_id`) REFERENCES `customer_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dashboard.customers: ~10 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` (`id`, `customer_status_id`, `name`) VALUES
	(1, 2, 'Vandervort PLC'),
	(2, 2, 'Klocko Inc'),
	(3, 1, 'Schmitt Ltd'),
	(4, 1, 'Hermiston, Metz and Kirlin'),
	(5, 1, 'Howe, Koss and Jakubowski'),
	(6, 1, 'Ankunding and Sons'),
	(7, 1, 'Wehner Group'),
	(8, 1, 'Breitenberg-O\'Reilly'),
	(9, 1, 'Fadel and Sons'),
	(10, 1, 'Kuhn Ltd');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table dashboard.customer_status
CREATE TABLE IF NOT EXISTS `customer_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_status_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dashboard.customer_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `customer_status` DISABLE KEYS */;
INSERT INTO `customer_status` (`id`, `code`, `name`) VALUES
	(1, 'AC', 'Active'),
	(2, 'RE', 'Removed');
/*!40000 ALTER TABLE `customer_status` ENABLE KEYS */;

-- Dumping structure for table dashboard.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dashboard.migrations: ~1 rows (approximately)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(19, '2020_07_20_083724_create_customer_status_table', 1),
	(20, '2020_07_20_084318_create_customers_table', 1),
	(21, '2020_07_20_084846_create_order_status_table', 1),
	(22, '2020_07_20_084929_create_orders_table', 1),
	(23, '2020_07_20_104359_seed_customer_satus_values', 1),
	(24, '2020_07_20_105112_seed_order_status_values', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table dashboard.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint unsigned NOT NULL,
  `order_status_id` bigint unsigned NOT NULL,
  `total` int unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `orders_customer_id_foreign` (`customer_id`),
  KEY `orders_order_status_id_foreign` (`order_status_id`),
  CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `orders_order_status_id_foreign` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dashboard.orders: ~0 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `customer_id`, `order_status_id`, `total`, `created_at`) VALUES
	(1, 2, 2, 10000, '2020-05-20 13:05:51'),
	(2, 2, 1, 10000, '2020-03-20 13:05:51'),
	(3, 4, 2, 10000, '2019-05-20 13:05:51'),
	(4, 4, 1, 10000, '2020-03-20 13:05:51'),
	(5, 5, 2, 5000, '2020-06-20 13:05:51'),
	(6, 5, 2, 10000, '2020-05-20 13:05:51'),
	(7, 5, 2, 75000, '2020-05-20 13:05:51'),
	(8, 6, 2, 15000, '2020-06-20 13:05:51'),
	(9, 6, 2, 10000, '2020-05-20 13:05:51'),
	(10, 6, 2, 10000, '2020-03-20 13:05:51'),
	(11, 7, 2, 15000, '2020-06-20 13:05:51'),
	(12, 7, 2, 10000, '2020-05-20 13:05:51'),
	(13, 8, 1, 15000, '2020-06-20 13:05:51'),
	(14, 8, 2, 10000, '2020-05-20 13:05:51'),
	(15, 9, 2, 15000, '2020-03-20 13:05:51'),
	(16, 9, 2, 10000, '2020-02-20 13:05:51'),
	(17, 10, 1, 17000, '2020-06-20 13:05:51'),
	(18, 10, 1, 12000, '2020-05-20 13:05:51');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table dashboard.order_status
CREATE TABLE IF NOT EXISTS `order_status` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_status_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table dashboard.order_status: ~2 rows (approximately)
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` (`id`, `code`, `name`) VALUES
	(1, 'AC', 'Active'),
	(2, 'CO', 'Completed');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
