# Customer Order Dashboard

Project was built using a Dockerised development environment.

Implemented using Laravel (7.x) and PHP (7.4) against a MySQL (8.0) database.

## Project Structure

**/db** - Directory contains the local docker based database

**/db/scripts** - Seed file for establishing the local database state

**/src** - Directory containing the application source code

**/Dockerfile** - Dockerfile to build the application located in the *src* directory

## Docker

### Local Requirements

To run and/or develop locally [Docker Desktop](https://www.docker.com/products/docker-desktop) must be installed.



### Local Runtime

Build the required environment by executing the relevant commands in the project home directory.

NB: Windows users should use PowerShell

#### Development

Build the docker environment (mounts local filesystem):
```
docker-compose -f "docker-compose-dev.yml" up -d --build
```

Import Libraries (required before running)
```
docker exec -it dashboard_web /bin/sh -c "[ -e /bin/bash ] && composer install"
```
*It is required to import libraries as they will not initially exist on the mounted filesystem.*

#### Runtime only

Build the docker environment (does not mount local filesystem):
```
docker-compose -f "docker-compose.yml" up -d --build
```
*Libraries are installed during the container build*

## Usage

The project implements a single route running within a docker environment.

To access from a local setup navigate to: [http://localhost:4001/customer-orders](http://localhost:4001/customer-orders)


## Tests

Tests can be executed using:
```
docker exec -it dashboard_web /bin/sh -c "[ -e /bin/bash ] && php artisan test"
```
*Only some sample tests have been constructed*
