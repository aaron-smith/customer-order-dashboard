FROM php:7.4-apache

ENV ROOT_DIR             /var/www/html
ENV APACHE_DOCUMENT_ROOT /var/www/html/public

COPY src ${ROOT_DIR}
COPY src/.env.base ${ROOT_DIR}/.env
WORKDIR ${ROOT_DIR}

# PHP modules
RUN apt-get update && apt-get install -y \
        libmcrypt-dev \
        libpng-dev \
        zlib1g-dev \
        git \
        libzip-dev \
        zip \
        unzip \
        libxml2-dev \
    && docker-php-ext-install -j$(nproc) iconv pdo pdo_mysql zip soap \
    && docker-php-ext-install -j$(nproc) gd \
    && a2enmod rewrite \
    && a2enmod proxy_http \
    && apt-get clean \
    && mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

# PHP libraries (via composer)
RUN curl https://getcomposer.org/installer | php -- && mv composer.phar /usr/local/bin/composer \
    && composer install \
    && chown www-data:www-data -R ${ROOT_DIR} \
    && php artisan key:generate \
    && php artisan config:clear

# Let apache to find the redefined document root
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf \
    && sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
