<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Customer Orders</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
  </head>
  <body>
    <div class="container" style="padding-bottom: 2em; padding-top: 1em">
      <div class="row">
        <div class="col-12">
          <h1>Customer Orders</h1>
        </div>
      </div>
    </div>
    <div class="container">
      <table class="table">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Customer</th>
            <th scope="col" class="text-right"># Orders</th>
            <th scope="col" class="text-right">Total</th>
            <th scope="col" class="text-right">3 Month Total</th>
            <th scope="col" class="text-right">12 Month Total</th>
            <th scope="col" class="text-right"># Incomplete</th>
            <th scope="col" class="text-right">Incomplete Total</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($customers as $customer)
            <tr
              @switch (true)
                @case (!$customer->active)
                  class="table-danger"
                  @break
                @case ($customer->orders()->completed()->withinMonths(12)->sum('total') == 0)
                  class="table-warning"
                  @break
                @case ($customer->orders()->completed()->withinMonths(3)->sum('total') >= 20000)
                  class="table-success"
                  @break
              @endswitch
            >
              <td scope="col">{{ $customer->name }}</td>
              <td scope="col" class="text-right">{{ $customer->orders()->completed()->count() }}</td>
              <td scope="col" class="text-right">${{ number_format($customer->orders()->completed()->sum('total') / 100, 2) }}</td>
              <td scope="col" class="text-right">${{ number_format($customer->orders()->completed()->withinMonths(3)->sum('total') / 100, 2) }}</td>
              <td scope="col" class="text-right">${{ number_format($customer->orders()->completed()->withinMonths(12)->sum('total') / 100, 2) }}</td>
              <td scope="col" class="text-right">{{ $customer->orders()->active()->count() }}</td>
              <td scope="col" class="text-right">${{ number_format($customer->orders()->active()->sum('total') / 100, 2) }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    {{-- Legend table --}}
    <div class="container">
      <table class="table-responsive table-sm">
        <thead class="thead-light">
          <tr>
            <th scope="col">Key</th>
          </tr>
        </thead>
        <tbody>
          <tr class="table-danger">
            <td>Removed Customer</td>
          </tr>
          <tr class="table-warning">
            <td>No orders in last 12 months</td>
          </tr>
          <tr class="table-success">
            <td>Minimum $200.00 in sales in last 3 months</td>
          </tr>
        </tbody>
      </table>
    </div>
  </body>
</html>
