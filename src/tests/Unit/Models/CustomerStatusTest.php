<?php

namespace Tests\Unit\Models;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use \App\Models\CustomerStatus;

class CustomerStatusTest extends TestCase
{
    use RefreshDatabase;

    const TABLE='customer_status';

    /**
     * Test seed data has been created ok.
     *
     * @return void
     */
    public function testSeedDataAvailable()
    {
        $this->assertDatabaseHas(self::TABLE, [
            'code' => CustomerStatus::STATUS_ACTIVE,
        ]);

        $this->assertDatabaseHas(self::TABLE, [
            'code' => CustomerStatus::STATUS_REMOVED,
        ]);
    }

    /**
     * Test scopes.
     *
     * @return void
     */
    public function testScopes()
    {
        $active = CustomerStatus::active()->first();
        $this->assertNotEmpty($active);
        $this->assertEquals($active->code, CustomerStatus::STATUS_ACTIVE);

        $removed = CustomerStatus::removed()->first();
        $this->assertNotEmpty($removed);
        $this->assertEquals($removed->code, CustomerStatus::STATUS_REMOVED);
    }

    /**
     * Test relations.
     *
     * @return void
     */
    public function testCustomersRelation()
    {
        $this->seed();

        $customer_count = CustomerStatus::removed()->first()->customers()->count();
        $this->assertEquals($customer_count, 2);
    }
}
