<?php

use Illuminate\Database\Seeder;

use App\Models\CustomerStatus;
use App\Models\OrderStatus;

use Carbon\Carbon;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Status id's
        $active_customer_status = CustomerStatus::where('code', CustomerStatus::STATUS_ACTIVE)->firstOrFail();
        $removed_customer_status = CustomerStatus::where('code', CustomerStatus::STATUS_REMOVED)->firstOrFail();

        $active_order_status = OrderStatus::where('code', OrderStatus::STATUS_ACTIVE)->firstOrFail();
        $completed_order_status = OrderStatus::where('code', OrderStatus::STATUS_COMPLETED)->firstOrFail();

        // Create the customer
        foreach ($this->seed_data() as $data) {
            if ($data['status'] == CustomerStatus::STATUS_ACTIVE) {
                $customer_status_id = $active_customer_status->id;
            } else {
                $customer_status_id = $removed_customer_status->id;
            }
            $customer = factory(App\Models\Customer::class)->create([
                'customer_status_id' => $customer_status_id,
            ]);

            // Customer Orders
            foreach ($data['orders'] as $order) {
                if ($order['status'] == OrderStatus::STATUS_ACTIVE) {
                    $order_status_id = $active_order_status->id;
                } else {
                    $order_status_id = $completed_order_status->id;
                }
                $order_date = Carbon::now('UTC')->subMonth($order['months_old']);
                factory(App\Models\Order::class)->create([
                    'customer_id'     => $customer->id,
                    'order_status_id' => $order_status_id,
                    'total'           => $order['total'],
                    'created_at'      => $order_date,
                ]);
            }
        }
    }

    /**
     * The data to seed
     *
     * @return array
     */
    private function seed_data()
    {
        return [
            [
                // #1 Removed, no orders
                'status' => CustomerStatus::STATUS_REMOVED,
                'orders' => [],
            ],
            [
                // #2 Removed, some orders
                'status' => CustomerStatus::STATUS_REMOVED,
                'orders' => [
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                    [
                        'months_old' => 4,
                        'status'     => OrderStatus::STATUS_ACTIVE,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #3 Active, no orders
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [],
            ],
            [
                // #4 Active, no orders in last 12 months
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 14,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                    [
                        'months_old' => 4,
                        'status'     => OrderStatus::STATUS_ACTIVE,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #5  Active min 200 in last 3 months
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 1,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 5000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 75000,
                    ],
                ],
            ],
            [
                // #6  Active 200 last 3 months, 100 > 3 months
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 1,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 15000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                    [
                        'months_old' => 4,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #7  Active 200 last 3 months
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 1,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 15000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #8  Active 200 last 3 months (only 100 completed)
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 1,
                        'status'     => OrderStatus::STATUS_ACTIVE,
                        'total'      => 15000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #9  Active 200 > last 3 months
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 4,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 15000,
                    ],
                    [
                        'months_old' => 5,
                        'status'     => OrderStatus::STATUS_COMPLETED,
                        'total'      => 10000,
                    ],
                ],
            ],
            [
                // #9  Active 200 last 3 months (All imcomplete)
                'status' => CustomerStatus::STATUS_ACTIVE,
                'orders' => [
                    [
                        'months_old' => 1,
                        'status'     => OrderStatus::STATUS_ACTIVE,
                        'total'      => 17000,
                    ],
                    [
                        'months_old' => 2,
                        'status'     => OrderStatus::STATUS_ACTIVE,
                        'total'      => 12000,
                    ],
                ],
            ],
        ];
    }
}
