<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use App\Models\OrderStatus;

class SeedOrderStatusValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->seed_data() as $data) {
            OrderStatus::create([
                'code' => $data['code'],
                'name' => $data['name'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach ($this->seed_data() as $data) {
            OrderStatus::where('code', $data['code'])
                ->delete();
        }
    }

    /**
     * The data to seed in the migration
     *
     * @return array
     */
    private function seed_data()
    {
        return [
            [
                'code' => OrderStatus::STATUS_ACTIVE,
                'name' => 'Active',
            ],
            [
                'code' => OrderStatus::STATUS_COMPLETED,
                'name' => 'Completed',
            ],
        ];
    }
}
