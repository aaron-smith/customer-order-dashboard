<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CustomerStatus;
use Faker\Generator as Faker;

$factory->define(CustomerStatus::class, function (Faker $faker) {
    return [
        'code' => $faker->randomElement([
            CustomerStatus::STATUS_ACTIVE,
            CustomerStatus::STATUS_REMOVED,
        ]),
        'name' => $faker->word,
    ];
});
