<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OrderStatus;
use Faker\Generator as Faker;

$factory->define(OrderStatus::class, function (Faker $faker) {
    return [
        'code' => $faker->randomElement([
            OrderStatus::STATUS_ACTIVE,
            OrderStatus::STATUS_COMPLETED,
        ]),
        'name' => $faker->word,
    ];
});
