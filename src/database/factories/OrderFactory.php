<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use App\Models\OrderStatus;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    $order_status_ids = OrderStatus::select('id')
        ->get();
    $order_status_array = [];
    foreach ($order_status_ids as $status) {
        $order_status_array[] = $status->id;
    }
    return [
        'customer_id'     => $faker->randomDigit,
        'order_status_id' => $faker->randomElement($order_status_ids),
        'total'           => $faker->numberBetween($min = 5000, $max = 10000),
        'created_at'      => $faker->dateTimeBetween($startDate = '-2 years', $endDate = 'now', $timezone = 'UTC')
    ];
});
