<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Customer;
use App\Models\CustomerStatus;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {
    $customer_status_ids = CustomerStatus::select('id')
        ->get();
    $customer_status_array = [];
    foreach ($customer_status_ids as $status) {
        $customer_status_array[] = $status->id;
    }
    return [
        'customer_status_id' => $faker->randomElement($customer_status_array),
        'name'               => $faker->company,
    ];
});
