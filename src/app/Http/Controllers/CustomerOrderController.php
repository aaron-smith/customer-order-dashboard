<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Customer;

class CustomerOrderController extends Controller
{
    /**
     * Return the requested pet.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $customers = Customer::orderBy('name')->get();

        return response()
            ->view('dashboard/customer/orders', [
                'customers' => $customers,
            ]);
    }
}
