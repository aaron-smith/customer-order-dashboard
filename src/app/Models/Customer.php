<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'customer_status_id',
        'name',
    ];

    public $timestamps = false;

    // Relations
    // =======================================================================

    public function status()
    {
        return $this->hasOne('App\Models\CustomerStatus', 'id', 'customer_status_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id', 'id');
    }

    // Scopes
    // =======================================================================

    public function scopeActive($query)
    {
        return $query->whereHas('status', function($q) {
            $q->active();
        });
    }

    public function scopeRemoved($query)
    {
        return $query->whereHas('status', function($q) {
            $q->removed();
        });
    }

    // Mutators
    // =======================================================================

    public function getActiveAttribute()
    {
        return $this->status()->active()->count() != 0;
    }

    public function getRemovedAttibute()
    {
        return $this->status()->removed()->count() != 0;
    }

}
