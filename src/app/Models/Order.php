<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'customer_id',
        'order_status_id',
        'total',
        'created_at',
    ];

    public $timestamps = false;
    
    protected $dates = [
        'created_at',
    ];

    // Relations
    // =======================================================================

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'id', 'cusomter_id');
    }

    public function status()
    {
        return $this->hasOne('App\Models\OrderStatus', 'id', 'order_status_id');
    }

     // Scopes
    // =======================================================================

    public function scopeActive($query)
    {
        return $query->whereHas('status', function($q) {
            $q->active();
        });
    }

    public function scopeCompleted($query)
    {
        return $query->whereHas('status', function($q) {
            $q->completed();
        });
    }

    public function scopeWithinMonths($query, $months)
    {
        return $query->where('created_at', '>=', Carbon::now()->subMonths($months));
    }

    // Mutators
    // =======================================================================

    public function getActiveAttribute()
    {
        return $this->status()->active()->count() != 0;
    }

    public function getCompletedAttibute()
    {
        return $this->status()->completed()->count() != 0;
    }
}
