<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    const STATUS_ACTIVE    = 'AC';
    const STATUS_COMPLETED = 'CO';

    protected $table = 'order_status';

    protected $fillable = [
        'code',
        'name',
    ];

    public $timestamps = false;

    // Relations
    // =======================================================================

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'order_status_id', 'id');
    }
    
    // Scopes
    // =======================================================================

    public function scopeActive($query)
    {
        return $query->where('code', self::STATUS_ACTIVE);
    }

    public function scopeCompleted($query)
    {
        return $query->where('code', self::STATUS_COMPLETED);
    }

}
