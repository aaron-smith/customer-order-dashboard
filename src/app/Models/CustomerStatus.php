<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerStatus extends Model
{
    const STATUS_ACTIVE  = 'AC';
    const STATUS_REMOVED = 'RE';

    protected $table = 'customer_status';

    protected $fillable = [
        'code',
        'name',
    ];

    public $timestamps = false;

    // Relations
    // =======================================================================

    public function customers()
    {
        return $this->hasMany('App\Models\Customer', 'customer_status_id', 'id');
    }
    
    // Scopes
    // =======================================================================

    public function scopeActive($query)
    {
        return $query->where('code', self::STATUS_ACTIVE);
    }

    public function scopeRemoved($query)
    {
        return $query->where('code', self::STATUS_REMOVED);
    }
}
